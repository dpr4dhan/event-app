<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filter = $request->input('filter');

        if(!empty($filter)){
            $query = DB::table('events');
            $today = date('Y-m-d');
            switch($filter){
                case 'finished_event':
                    $query->where('end_date', '<',  $today);
                    break;
                case 'upcoming_event':
                    $query->where('start_date', '>=',  $today);
                    break;
                case 'upcoming_event_7days':
                    $to_date = date('Y-m-d', strtotime('+7 day', strtotime($today)));
                    $query->whereBetween('start_date', [$today, $to_date]);
                    break;
                case 'finished_event_7days':
                    $from_date = date('Y-m-d', strtotime('-7 day', strtotime($today)));
                    $query->whereBetween('end_date', [$from_date, $today]);
                    break;
                default:
                    $query->where('start_date', '=',  '0000-00-00');
            }
            $events = $query->orderBy('start_date')->get();
        }else{
            $events = Event::orderBy('start_date')->get();
        }

        return response()->json(['success'=>true, 'events' => $events]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'description' => 'required'
        ]);
        try{
            $event = Event::create($request->post());
            return response()->json([
                'success' => true,
                'message'=>'Event Created Successfully!!',
                'event'=>$event
            ], 201);
        }catch(\Exception $e){
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
        return response()->json(['success'=>true, 'event'=> $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'description' => 'required'
        ]);
        try{
            $event->fill($request->post())->save();
            return response()->json([
                'success' => true,
                'message'=>'Event Updated Successfully!!',
                'event'=>$event
            ]);
        }catch(\Exception $e){
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
        try{
            $event->delete();
            return response()->json([
                'success'=> true,
                'message'=>'Event Deleted Successfully!!'
            ]);
        }catch(\Exception $e){
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }

    }
}
