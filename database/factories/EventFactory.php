<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start_date =  $this->faker->dateTimeThisMonth()->format('Y-m-d');
        $to_date = date('Y-m-d', strtotime('+7 day', strtotime($start_date)));
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text(100),
            'start_date' => $start_date,
            'end_date' => $to_date,
        ];
    }
}
