const Welcome = () => import('./components/Welcome.vue' /* webpackChunkName: "resource/js/components/welcome" */)
const EventList = () => import('./components/event/List.vue' /* webpackChunkName: "resource/js/components/event/list" */)
const EventCreate = () => import('./components/event/Add.vue' /* webpackChunkName: "resource/js/components/event/add" */)
const EventEdit = () => import('./components/event/Edit.vue' /* webpackChunkName: "resource/js/components/event/edit" */)

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Welcome
    },
    {
        name: 'eventList',
        path: '/event',
        component: EventList
    },
    {
        name: 'eventEdit',
        path: '/event/:id/edit',
        component: EventEdit
    },
    {
        name: 'eventAdd',
        path: '/event/add',
        component: EventCreate
    }

]
