<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Faker\Factory;
use App\Models\Event;

class EventTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * Event listing api.
     *
     * @return void
     */
    public function test_event_list_api()
    {
       //Create fake from factory
       // $event = Event::factory()->create();

        //When user visit the event listing api
        $response = $this->get('/api/event');

        //api must return success true with events
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    /**
     * event create api.
     *
     * @return void
     */
    public function test_event_create_api()
    {

        $faker = Factory::create();
        $start_date = $faker->dateTimeThisMonth()->format('Y-m-d');
        $to_date = date('Y-m-d', strtotime('+7 day', strtotime($start_date)));
        $eventData = [
            'title' => $faker->sentence,
            'description' => $faker->text(100),
            'start_date' => $start_date,
            'end_date' => $to_date
        ];
        //calling create event api
        $response = $this->post('/api/event', $eventData);

        //api must return success true with event
        $response->assertStatus(201)
            ->assertJson([
                'success' => true
            ]);
    }


    /**
     * event detail api.
     *
     * @return void
     */
    public function test_event_detail_api()
    {

        //assuming there is event in table
        $event = Event::first();
        $event_id = $event ? $event->id : '';
        $response = $this->get('/api/event/'.$event_id);

        //api must return success true with events
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    /**
     * event update api.
     *
     * @return void
     */
    public function test_event_update_api()
    {
        $faker = Factory::create();
        $start_date = $faker->dateTimeThisMonth()->format('Y-m-d');
        $to_date = date('Y-m-d', strtotime('+7 day', strtotime($start_date)));
        $eventData = [
            'title' => $faker->sentence,
            'description' => $faker->text(100),
            'start_date' => $start_date,
            'end_date' => $to_date
        ];

        //assuming there is event in table
        $event = Event::first();
        $event_id = $event ? $event->id : '';
        $response = $this->patch('/api/event/'.$event_id, $eventData);

        //api must return success true with event
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    /**
     * event delete api.
     */
    public function test_delete_event_api(){
        // assuming theres events in table
        $event = Event::first();
        $event_id = $event ? $event->id : '';
        //assuming there is event in table
        $response = $this->delete('/api/event/'. $event_id);

        //api must return success true with event
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}
